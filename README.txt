CONTENTS OF THIS FILE
---------------------

Module Name:
------------
Node Reference Subqueue

Description
-----------
This module allows users to create a node reference sub-queue from the 
selected content types to have a unique sub-queue. 

You can place nodes into any of these sub-queues based on which node reference 
that node has been tagged with. You can select a node reference field and 
associated node type to create sub queue for each node-reference item.

Dependencies:
-------------
nodequeue
nodereference


How to use this module?
-----------------------
- Download and install node reference subqueue module from drupal.org
- You must have both nodequeue and node reference modules installed to run this.
- After the module setup, you will find a link "Add node reference subqueue" on
  the nodequeues list page.
- You can now create a subqueue for the node references field and associated 
  content type.
- Your node reference subqueues will be listed on the nodequeues main page. Here
  you can manage  the order of nodes under the tagged nodereference subqueue.
- Enjoy! And feedback is always welcome!

Note:
-----
This module does not work entity reference fields. It only work with
node reference (Node entity).

Author
------
Icreon Communications Pvt. Ltd. <icreon.com>
